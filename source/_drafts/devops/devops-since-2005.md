---
title: DevOps since 2005
date: 2023-02-01T07:00:00.000Z
author: 'Pietro Martinelli'
tags: [devops, me, job]
categories:
    - devops
---
# Episodes
- Deploying a PHP app to two different environments (thanks to Nicola Gatta for Webmin plugin) using custom a bunch of ant-based scripts
- Packaging a product for fifteen different customers (providing different DBMS, auth, data services)
- Simplifying build-and-deploy pipeline with GitLab *manual* tasks
- Continuous Deploy/Delivery of libraries
- Continuous Deploy/Delivery of services
- Migrating from GitLab to GitHub

# Lessons learned
- You can automate (almost) everything
- The simpler, the better
- Version numbers are for (and by) humans (The simpler, the better)
- Don't alter code repository from the pipeline (The simpler, the better)
- GitLab is (far) better than GitHub (The simpler, the better)
    - Containers everywhere
    - Wiki - subdirectories
    - No action-to-runtime dependencies
    - Built-in support for recursive checkout
    - Code checkout out-of-the-box
- Containers are your friends (The simpler, the better)
- Don't use *special* branches to choose different pipeline workflows
    - Tag commits when you whant to release
