---
title: Internal standards kill the team
date: 2023-02-05T07:00:00.000Z
author: 'Pietro Martinelli'
tags: [team]
categories:
    - team
---
So, the title of this post is purposely provocative: the *real* title should be "_**Low level** internal standards kill the team's **mood and creativity**_". Better? Ok!

## The context

All developers like *reuse*: reuse of code, *off course*, but reuse of *ideas*, too.
Reuse of *whatever* they are confident with: languages, frameworks, libraries, methodologies, daily practices in writing code, codebase organization, branching patterns, board layout, familiar editors and IDEs, command line tools and script... you got the point.

**This is good**, because *reuse* often means *time saving*.
*Code reuse* almost always means higher degree of astraction, too - then higher (internal) quality, and internal **quality is good**!
And using the same language, the same framework, the same *whatever-you-want* in different projects means **less wasted time** when moving from one to another - and this **is also good**.

## The problem
So, what's the matter with such a habit?


## Flavours

- *My* own library (working *not so well*)
- This (old and fixed) version of that framework
- That horrible static *logger*
- A fixed set of architectural *layers*, enforced by code generation
- An *one-size-fits-all* codebase structure *that works for me*
- Naming convention for variables
- Code style arbitrary rules

## (Maybe) The solution

## References
- [Template Driven Development: Why it doesn't work](https://dev.to/barryosull/template-driven-development-why-it-doesn-t-work-3c2f)