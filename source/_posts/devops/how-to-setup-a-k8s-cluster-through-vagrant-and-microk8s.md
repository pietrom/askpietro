---
title: How to setup a Kubernetes cluster on virtual machines through Vagrant and Microk8s
date: 2023-08-28 18:00:00
tags: [devops, vagrant, kubernetes, k8s]
categories:
    - devops
author: 'Pietro Martinelli'
---
Kubernetes (K8S) is for a few years now a trending topic. If you are approaching it, you need a way to test what you're learning - the usual way for beginners consists in the use of [minikube](https://minikube.sigs.k8s.io/docs), an *out-of-the-box* solution that sets up a single-node K8S cluster you can use for learning purposes, tipically through virtualization (but deploy on container and bare-metal is supported).

If you want to experiment with a production-like multi-node cluster, you have to find another solution - tipically you end up using a cloud provider supporting free subscription, like [okteto](https://www.okteto.com), or consuming your free initial credit on something like [GKE](https://cloud.google.com/kubernetes-engine), [EKS](https://aws.amazon.com/eks/), or [AKS](https://azure.microsoft.com/en-us/products/kubernetes-service).

In the past three years I've explored another approach, installing a K8S cluster on a group of VMs running on my physical machine.

I initially tried installing and configuring *from scratch* everything I needed - very interesting way of learning, but very annoying way to proceed if you need a running cluster in minutes: you need to choose and install the OS, pick and install one container manager (e.g. `containerd`), install official K8S packages (`kubelet`, `kubeadm`), disable the swap (for real!), check and fix firewall's rules, configure a `cgroupdriver`, restart a bunch of system daemons, ... then you are ready to set up the cluster, starting a *primary* and at least one *worker* node, choosing and installing a *network plugin*, a *metrics plugin*, an *ingress plugin*... easy, right?

If you want *play* with the cluster **without** entering the maze of system installation and configuration, the way to go is to setup your cluster through one *Kubernetes Distribution* - something like [MicroK8s](https://microk8s.io) or [K3s](https://k3s.io).

And if you're an automation addicted like me, may be you end up with a set of parameterizable script you can use to setup a cluster with a handful of CLI command: [this](https://gitlab.com/pietrom/vagrant-microk8s-cluster) is my version ot *the game* - you can try opening a terminal and lunching

```bash
git clone https://gitlab.com/pietrom/vagrant-microk8s-cluster.git
cd vagrant-microk8s-cluster
vagrant up mercury0
vagrant up mercury1
vagrant up mercury2
...
```

Configuration (for cluster name, VMs' IPs, ...) is available editing variables in `Vagrantfile`.

The provided solution works with [Vagrant](https://www.vagrantup.com) and a virtualization provider supported by the vagrant box [`generic/ubuntu2004`](https://app.vagrantup.com/generic/boxes/ubuntu2004).

Good luck and enjoy playing with you brand new K8S cluster!!