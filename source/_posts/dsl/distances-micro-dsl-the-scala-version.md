---
title: 'Distances micro-DSL - the Scala version'
date: 2022-01-25T18:15:00.000Z
alias:
    - 2022/01/25/distances-micro-dsl-the-scala-version/index.html
    - distances-micro-dsl-the-scala-version/index.html
author: 'Pietro Martinelli'
tags: [scala, dsl]
categories:
    - scala
---
I recently [wrote](/the-beatutiness-of-kotlin-episode-0) about implementing in Kotlin a very simple DSL for expressing distances. Here is its Scala version:

```scala
object Main {
    import Distance._

    def main(args: Array[String]) {
      val marathon = 42.km + 195.m + 30.cm
      println("Marathon " + marathon)
   }
}

case class Distance(val m: Double) {
    def + (that: Distance) = Distance(this.m + that.m)
}

object Distance {
    implicit class IntDistanceExtension(val value: Int) {
        def m = new Distance(value.toDouble)

        def km = new Distance(value.toDouble * 1000)

        def cm = new Distance(value.toDouble/ 100)
    }
}
```
