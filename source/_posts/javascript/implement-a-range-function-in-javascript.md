---
title: Implement a range function in JavaScript
date: 2021-12-17 07:40:52
alias:
    - 2021/12/17/implement-a-range-function-in-javascript/index.html
    - implement-a-range-function-in-javascript/index.html
tags: [javascript, functional programming]
categories:
    - functional programming
author: 'Pietro Martinelli'
---
I like to define a simple `range` function in my JavaScript projects, in order to easily adopt a functional approach when I need to work with integer *intervals*:
```javascript
function range(start, end) {
  const right = end || start
  const left = end && start || 0
  return Array.from({length: right - left}, (x, i) => left + i)
  // Alternative implementation:
  // return Array(right - left).fill(0).map((x, i) => left + i)
}
```
Basic usage:
```javascript
range(0, 10)
// [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

range(5, 10)
// [5, 6, 7, 8, 9]

range(7)
// [0, 1, 2, 3, 4, 5, 6]
```
