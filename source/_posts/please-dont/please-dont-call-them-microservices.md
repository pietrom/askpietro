---
title: Please, don't call them microservices
author: Pietro Martinelli
categories:
  - please don't...
date: 2022-01-02 11:05:00
alias:
  - 2022/01/02/please-dont-call-them-microservices/index.html
  - please-dont-call-them-microservices/index.html
tags: [design, microservices]
---
*Microservice* - the most overused word of the last ten years in software engineering...
We all love *microservices* - in words if nothing else.
But are we *really* speaking about _**micro**services_ ? Is this new distributed system consisting of microservices or is it a *distributed monolith*? And what's the difference?

Certainly we have all heard phrases like these:
- *Please, create a microservice that does **this** and **that** and **that** and **that**... and that...*
- *Well, here you need two microservices: the first one should create this table and read from it, the second one should consume that message and write its content into the same table...*
- *Let deploy these five microservices, in order to make the new feature available: beware that you should deploy the service A first, then B and C, then D, and only when the first four services will have been deployed you wil able to deploy the service E...*

We could continue, but... you got the point: not all of what we regularly call microservices really are: maybe they are *services*, but for sure they are not so *micro*.
Each of us undoubtedly has a very personal, opinionated list of characteristics that a true _**micro**service_ should not exhibit - this is mine (in increasing order of severity), at least at the time of writing: please, don't call them "microservices" if

1. they are developed by the same team
1. they are forced to share the same tech stack
1. they are forced to share libraries, e.g. for contract definitions
1. they are forced to be updated/deployed at the same time
1. they share code (not as external libraries), even infrastructural one
1. they share code implementing business logic (a special case for the previous case, but the most dangerous one)
1. they share the database (*coupling* in *space*)
1. they call each another through synchronous API (*coupling* in *time*)
1. the delivery of a new feature always requires a coordinated changes to more than one service (*functional coupling*)
1. ... ... ...
