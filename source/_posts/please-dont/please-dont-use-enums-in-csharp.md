---
title: Please, don't use enums in C#
author: Pietro Martinelli
categories:
  - please don't...
date: 2021-11-20 16:05:00
alias:
  - 2021/11/20/please-dont-use-enums-in-csharp/index.html
  - please-dont-use-enums-in-csharp
tags: [design, csharp, c#, modelling]
---
## Enumerated types
*Enumerated (discrete) types* are a powerful *modeling* tool for software developers: they allows them to explicitly state all and only the permitted values a variable can hold, with guarantee that
- no invalid values can be pushed into a function, and
- conditional (switch/case or pattern-matching based) depending on enumerated types can be recognized to be exhaustive by compilers.

This is strictly true for *enum*s you can define in languages like Scala (`case class`es), Kotlin (`enum`s) or even the old, mistreated Java (`enum`s), but is only an unmaintained, misleading promise for C#'s `enum`s.

## The problem (or "The C# way" to `enum`s")
Defining an `enum` in C# indeed is only a *syntactic sugar* you can leverage to define related, "namespaced" integer constants:
```csharp
public enum Ordinal {
  First = 1, Second = 2, Third = 3
}
```
is in essence only a *shortcut* for
```csharp
public static class Ordinal {
  public const int First = 1;
  public const int Second = 2;
  public const int Third = 3;
}
```
I'm not saying the compiler produces the same output - I'm saying in both cases you can refer to something like `Ordinal.Second` in order to get an `int` constant whose value is `2`.

### Issue #1
**No way to** define a method, say
```csharp
void DoSomething(Ordinal o) {
  Console.WriteLine($"Ordinal value is {o:D}");
}
```
**preventing** callers **to pass invalid values** into:
```csharp
DoSomething(Ordinal.First);
DoSomething((Ordinal)500);
```
is definitely valid code (from the compiler's point of view) producing the following output:
```
Ordinal value is 1
Ordinal value is 500 // WTF??? Value not present in Ordinal declaration...
```

### Issue #2
**No way to** rely on compiler in order to **check exhaustiveness of conditional checks**: you can indeed write
```csharp
public int Foo(Ordinal o) => o switch {
    Ordinal.First => 1,
    Ordinal.Second => 2,
    Ordinal.Third => 3,
};
```
but the compilers gives you a warning like *The switch expression does not handle all possible inputs (is is not exhaustive)*, even if all values defined by the `enum` are explicitly treated; in order to avoid this inappropriate warning you must add a fourth, never used branch to the  `switch`:
```csharp
_ => throw new Exception("Unexpected value")
```
(or you can return a *special value*, if you like *code smells* ;-)...).

So, C#'s `enum`s are syntactic sugar for `int` constants, and defining a method parameter of type `Ordinal` is nothing different from defining it of type `int` (yes, you can define an `enum` having `byte` or `long` or ${other integral type} as underlying representation (see [here](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/language-specification/enums#enum-declarations)), but... you got the idea).

From a modelling point of view, C#'s `enum`s are a **very poor feature**, which does not allow developers to define true *enumerated (discrete) types*: so... **please, don't use them**, or at least don't use them as if they were.

## The right way
The right way to model *enumerated/discrete types* in C# is imho to adopt a pattern that I first heard about in 2004, reading [Hardcore Java](https://www.oreilly.com/library/view/hardcore-java/0596005687) enlightening book:
```
public sealed class Ordinal {
    public int Value { get; }
    private Ordinal(int value) { Value = value; }
    public static Ordinal First = new Ordinal2(0);
    public static Ordinal Second = new Ordinal2(1);
    public static Ordinal Third = new Ordinal2(2);
}
```
This does not solve the problem of exhaustiveness' check, but models true discrete type allowing only intended values to be used where `Ordinal` parameters are required.

A variant of this pattern, based on inheritance, allow developers to attach polymorphic behaviours to *enumeration* cases.

## Bonus (or "The Java way to `enum`s")
By the way, this is the way `enum`'s implementation in Java (since 2004!!!) and Kotlin work: they provide substantially a *syntactic sugar* for the pattern above, allowing true *enumerated/discrete types* modelling:
```java
public enum Ordinal {
    First {
        @Override
        void doSomething() {
          // Behaviour for case First
        }
    }, Second {
        @Override
        void doSomething() {
          // Behaviour for case Second
        }
    }, Third {
        @Override
        void doSomething() {
          // Behaviour for case Third
        }
    };

    abstract void doSomething();
}
```
Java supports exhaustiveness check out of the box, too:
```java
void doSomething(Ordinal o) {
  var value = switch (o) { // compiler's error: 'switch' expression does not cover all possible input values
      case First -> 1;
      case Second -> 2;
  };
}

void doSomething(Ordinal o) {
  var value = switch (o) { // no errors, no warning
      case First -> 1;
      case Second -> 2;
      case Third -> 3;
  };
}
```
