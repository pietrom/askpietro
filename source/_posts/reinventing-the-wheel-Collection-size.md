---
title: 'Reinventing the wheel: Collection.size()'
author: Pietro Martinelli
categories:
  - coding horrors
date: 2012-02-17 18:52:07
alias:
  - 2012/02/17/reinventing-the-wheel-Collection-size/index.html
tags: [coding horrors, java, reinventing the wheel]
---
Original way to reinvent the wheel - by an examination of "Programming Fundamentals"

```java 
public class MyContainer {
   private final Collection myBag;

   public int getBagSize() {
    int j = 0;
    for(int i = 0; i < this.myBag.size(); i++) {
     j++;
    }
    return j;
   }
}
```