---
title: 'Testing and shell-scripting reloaded: bash-unit'
categories:
  - testing
date: 2023-12-09T10:00:00.000Z
tags: [bash, scripting, shell, testing, tdd]
author: 'Pietro Martinelli'
---
I already wrote about testing bash scripts automatically [in this post](/testing/testing-and-shell-scripting/): if you missed it, reading it can be useful to get the *context*.
# A step forward
Moving a step forward, when you tests your bash scripts you can be interested both in *mocking* standard input and *spying* *standard output* (and *standard error*): here is how.
## *Spying* standard output and error
You can write a test case that checks an assertion about produced output simply exploiting *command substitution*: you can write a test case that collects *standard output* (into the `output` variable) and makes an assertion about its content.
```bash
test_spying_stdout () {
  output=$(echo -e "1 2 3 4 5 6")
  echo $output | grep "1 2 3 4 5 6" > /dev/null
}
```
Simply adding some redirection *stuff* you can do the same about *standard error*:
```bash
test_spying_stderr () {
  error=$(cat /proc/crypto no-file.txt  2>&1 > /dev/null)
  echo $error | grep "no-file.txt: No such file or directory" > /dev/null
}
```
## Mocking standard input
If you need to implement a test invoking a script that gets input from *stding*, both through redirection or through `read` command, you can provide mocked input providing a hardcoded version of *stdin* through `<<EOF` syntax:
```bash
test_mocking_stdin () {
  cat <<EOF | grep "AAA" > /dev/null
XXX
YYY
AAA
ZZZ
EOF
}
```
With
```bash
  cat <<EOF | your_command
...
...
EOF
```
you're instructing the shell to use the text stream provided **after the command invocation and before the closing `EOF` token** as `stdin`: now `your_command` can read from *stdin* getting the hardcoded, well known input you want to base your test case on.
# bash-unit
Reached this point, I think we can extract a slightly reworked version of the script I have shown in the [previous post](/testing/testing-and-shell-scripting/) into a reusable *tool* we can rely on to run our scripts' test cases. With little imagination I called it `bash-unit` and published it [here](https://gitlab.com/darwinsw/bash-unit).

Simply put, it allows you to simply launch
```bash
bash-unit [test_cases_dir]
```
in order to execute the test suite: you find full installation instructions and test cases samples in the [README file](https://gitlab.com/darwinsw/bash-unit/-/blob/main/README.md).